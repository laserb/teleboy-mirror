#!/usr/bin/python3
import os
import time
import yaml

from teleboy import Teleboy


class TeleboyMirror(Teleboy):
    def __init__(self):
        super(TeleboyMirror, self).__init__()

        self.username = None
        self.password = None
        self.media_dir = None
        self.state_dir = None
        self.recording_paths = {}

        self._load_config()
        self._create_media_folder()

    def _load_config(self):
        with open(os.path.expanduser("~/.teleboy")) as f:
            data = yaml.load(f)

        self.username = data["username"]
        self.password = data["password"]
        self.media_dir = data["media_dir"]
        self.state_dir = os.path.join(self.media_dir, ".states")

    def _create_media_folder(self):
        if self.media_dir and not os.path.exists(self.media_dir):
            os.mkdir(self.media_dir)
        if self.state_dir and not os.path.exists(self.state_dir):
            os.mkdir(self.state_dir)

    def _get_media_files(self):
        for path, folder in self._get_media_folders():
            for filename in os.listdir(path):
                yield path, filename

    def _get_media_folders(self):
        for folder in os.listdir(self.media_dir):
            if folder == ".states":
                continue
            path = os.path.join(self.media_dir, folder)
            yield path, folder

    def _get_state_files(self):
        for state in os.listdir(self.state_dir):
            path = os.path.join(self.state_dir, state)
            with open(path) as f:
                filename = f.read()
            recid = state.rsplit(".", 1)[0]
            yield recid, filename

    def _sanitize_filename(self, filename):
        replace_character = "_"
        bad_characters = ["/", ":", "?", "."]
        for char in bad_characters:
            filename = filename.replace(char, replace_character)
        return filename

    def get_filename(self, item):
        date = self._sanitize_filename(item["begin"])
        title = self._sanitize_filename(item["title"])
        subtitle = self._sanitize_filename(item["subtitle"])
        recid = item["id"]
        filename = "{}_{}_{}_{}.mp4".format(date, title, subtitle, recid)
        return filename

    def get_state(self, recid):
        return os.path.join(self.state_dir, "{}.ready".format(recid))

    def create_directories(self):
        for title in self.get_title_set():
            title = self._sanitize_filename(title)
            path = os.path.join(self.media_dir, title)
            if not os.path.exists(path):
                os.mkdir(path)

    def get_recordings(self):
        self.recording_paths = {}
        for path, recording in self._get_media_files():
            if recording.endswith(".mp4"):
                recid = os.path.splitext(recording)[0].rsplit("_", 1)[-1]
                self.recording_paths[recid] = os.path.join(path, recording)
        return self.recording_paths

    def remove_remote_deleted_recordings(self):
        for recid, path in self.recording_paths.items():
            if path is not None:
                print("Removing local {}".format(path))
                os.remove(path)
                state = self.get_state(recid)
                if os.path.exists(state):
                    os.remove(state)

    def remove_local_deleted_recordings(self):
        for recid, path in self._get_state_files():
            if not os.path.exists(path):
                try:
                    print("Removing remote {}".format(path))
                    self.delete(recid)
                    os.remove(self.get_state(recid))
                except Exception as e:
                    print(e)

    def download_missing_recordings(self):
        downloaded = False
        for recid, item in self.get_broadcasts().items():
            # touch still seen recordings
            self.recording_paths[str(recid)] = None

            # get filename and title
            filename = self.get_filename(item)
            title = item["title"]
            title = self._sanitize_filename(title)
            print("Checking {}".format(title))

            try:

                # download if the file does not exist yet
                path = os.path.join(self.media_dir, title, filename)
                state = self.get_state(recid)
                if not os.path.exists(path) and not os.path.exists(state):
                    print("Downloading {} to {}".format(title, path))
                    downloaded = True
                    partial_path = path + ".partial"
                    self.download(recid, partial_path)
                    os.rename(partial_path, path)
                with open(state, "w") as f:
                    f.write(path)
            except Exception as e:
                print("Downloading {} failed with error {}".format(title, e))
        return downloaded

    def clean_partial_downloads(self):
        for path, recording in self._get_media_files():
            if recording.endswith(".partial"):
                os.remove(os.path.join(path, recording))

    def clean_empty_directories(self):
        for path, folder in self._get_media_folders():
            if not os.listdir(path):
                os.rmdir(path)

    def sync(self):
        self.remove_local_deleted_recordings()
        self.update()
        self.clean_partial_downloads()
        self.create_directories()
        self.get_recordings()
        downloaded = self.download_missing_recordings()
        self.remove_remote_deleted_recordings()
        self.clean_empty_directories()
        return downloaded


def main():
    mirror = TeleboyMirror()
    while True:
        downloaded = mirror.sync()
        if downloaded:
            time.sleep(5)
        else:
            time.sleep(15*60)


if __name__ == "__main__":
    main()
