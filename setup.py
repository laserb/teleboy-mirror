#!/usr/bin/python3
from setuptools import setup

setup(name='teleboy-mirror',
      version='1.0',
      py_modules=['teleboy_mirror'],
      entry_points={
        'console_scripts': [
            'teleboy-mirror=teleboy_mirror:main'
            ],
      },
      install_requires=[
          'pyyaml',
          'python-teleboy',
      ]
      )
