import unittest
import os
import shutil
from unittest.mock import MagicMock, call
from teleboy_mirror import TeleboyMirror


MEDIA_DIR = "/tmp/teleboy-mirror-test"


def download(recid, path):
    with open(path, "w") as f:
        f.write(recid)


class SyncTestWithState(unittest.TestCase):
    def setUp(self):
        shutil.rmtree(MEDIA_DIR)
        TeleboyMirror._load_config = MagicMock()
        self.mirror = TeleboyMirror()
        self.mirror.username = "user"
        self.mirror.password = "password"
        self.mirror.media_dir = MEDIA_DIR
        self.mirror.state_dir = os.path.join(self.mirror.media_dir, ".state")

        self.mirror._create_media_folder()

        self.update_mock = MagicMock()
        self.mirror.update = self.update_mock
        self.download_mock = MagicMock(side_effect=download)
        self.mirror.download = self.download_mock
        self.delete_mock = MagicMock()
        self.mirror.delete = self.delete_mock

        self.item1 = {
                    "title": "first",
                    "id": 1,
                    "subtitle": "name",
                    "begin": "now"
                    }
        self.item2 = {
                    "title": "second",
                    "id": 2,
                    "subtitle": "other",
                    "begin": "then"
                    }
        self.item3 = {
                    "title": "second",
                    "id": 3,
                    "subtitle": "next",
                    "begin": "tomorrow"
                    }
        self.ready1 = os.path.join(self.mirror.state_dir, "1.ready")
        self.ready2 = os.path.join(self.mirror.state_dir, "2.ready")
        self.ready3 = os.path.join(self.mirror.state_dir, "3.ready")
        self.filepath1 = os.path.join(MEDIA_DIR,
                                      "first/now_first_name_1.mp4")
        self.filepath2 = os.path.join(MEDIA_DIR,
                                      "second/then_second_other_2.mp4")
        self.filepath3 = os.path.join(MEDIA_DIR,
                                      "second/now2_second_next_3.mp4")
        self.partialpath1 = self.filepath1 + ".partial"
        self.partialpath2 = self.filepath2 + ".partial"
        self.partialpath3 = self.filepath3 + ".partial"

    def test_download_missing(self):
        self.mirror.recordings = {
            "1": self.item1,
            "2": self.item2
        }
        self.download_mock.side_effect = download

        os.mkdir(os.path.join(MEDIA_DIR, "first"))
        os.mkdir(os.path.join(MEDIA_DIR, "second"))

        with open(self.ready1, "w") as f:
            f.write("1")

        self.mirror.download_missing_recordings()

        calls = [
            call("2", self.partialpath2)
        ]
        self.download_mock.assert_has_calls(calls, any_order=True)

        self.assertTrue(os.path.exists(self.filepath2))
        self.assertTrue(os.path.exists(self.ready1))
        self.assertTrue(os.path.exists(self.ready2))

    def test_remove_local_no_action(self):
        os.mkdir(os.path.join(MEDIA_DIR, "first"))
        with open(self.filepath1, "w") as f:
            f.write("1")
        with open(self.ready1, "w") as f:
            f.write(self.filepath1)
        self.mirror.remove_local_deleted_recordings()
        self.delete_mock.assert_not_called()

    def test_remove_local_delete(self):
        os.mkdir(os.path.join(MEDIA_DIR, "first"))
        with open(self.ready1, "w") as f:
            f.write(self.filepath1)
        self.mirror.remove_local_deleted_recordings()
        self.delete_mock.assert_called_once_with("1")

    def test_remove_remote(self):
        os.mkdir(os.path.join(MEDIA_DIR, "first"))
        with open(self.filepath1, "w") as f:
            f.write("1")
        with open(self.ready1, "w") as f:
            f.write(self.filepath1)
        self.mirror.recording_paths["1"] = self.filepath1
        self.mirror.remove_remote_deleted_recordings()
        self.assertFalse(os.path.exists(self.filepath1))
        self.assertFalse(os.path.exists(self.ready1))

    def test_sync(self):
        def update():
            self.mirror.titles = {
                "first": {"1": self.item1},
                "second": {"2": self.item2}
            }
            self.mirror.recordings = {
                "1": self.item1,
                "2": self.item2
            }

        self.update_mock.side_effect = update

        os.mkdir(os.path.join(MEDIA_DIR, "first"))
        os.mkdir(os.path.join(MEDIA_DIR, "second"))

        with open(self.filepath1, "w") as f:
            f.write("1")
        with open(self.ready1, "w") as f:
            f.write(self.filepath1)

        with open(self.ready3, "w") as f:
            f.write(self.filepath3)

        self.download_mock.side_effect = download

        self.mirror.sync()

        self.download_mock.assert_called_once_with("2", self.partialpath2)
        self.delete_mock.assert_called_once_with("3")

        self.assertTrue(os.path.exists(self.ready1))
        self.assertTrue(os.path.exists(self.ready2))
        self.assertFalse(os.path.exists(self.ready3))
