import unittest
import os
import shutil
from unittest.mock import MagicMock, patch, call
from teleboy_mirror import TeleboyMirror


MEDIA_DIR = "/tmp/teleboy-mirror-test"


class SyncTest(unittest.TestCase):
    def setUp(self):
        shutil.rmtree(MEDIA_DIR)
        TeleboyMirror._load_config = MagicMock()
        self.mirror = TeleboyMirror()
        self.mirror.username = "user"
        self.mirror.password = "password"
        self.mirror.media_dir = MEDIA_DIR
        self.mirror.state_dir = os.path.join(self.mirror.media_dir, ".state")

        self.mirror._create_media_folder()

        self.update_mock = MagicMock()
        self.mirror.update = self.update_mock
        self.download_mock = MagicMock()
        self.mirror.download = self.download_mock
        self.delete_mock = MagicMock()
        self.mirror.delete = self.delete_mock
        self.broadcasts_mock = MagicMock()
        self.mirror.get_broadcasts = self.broadcasts_mock

    @patch("os.remove")
    def test_clean_partial(self, remove_mock):
        def get_media_files():
            files = [
                ("first", "name_1.mp4"),
                ("second", "other_2.mp4.partial")
            ]
            for folder, filename in files:
                yield folder, filename

        self.mirror._get_media_files = MagicMock(side_effect=get_media_files)

        self.mirror.clean_partial_downloads()

        remove_mock.assert_called_once_with("second/other_2.mp4.partial")

    @patch("os.remove")
    def test_clean_partial_multi(self, remove_mock):
        def get_media_files():
            files = [
                ("first", "name_1.mp4"),
                ("first", "name_3.mp4.partial"),
                ("second", "other_2.mp4.partial")
            ]
            for folder, filename in files:
                yield folder, filename

        self.mirror._get_media_files = MagicMock(side_effect=get_media_files)

        self.mirror.clean_partial_downloads()
        calls = [call("second/other_2.mp4.partial"), call("first/name_3.mp4.partial")]
        remove_mock.assert_has_calls(calls, any_order=True)

    def test_get_recordings(self):
        def get_media_files():
            files = [
                ("first", "name_1.mp4"),
                ("second", "other_2.mp4")
            ]
            for folder, filename in files:
                yield folder, filename

        self.mirror._get_media_files = MagicMock(side_effect=get_media_files)

        recordings = self.mirror.get_recordings()

        expected = {
            "1": "first/name_1.mp4",
            "2": "second/other_2.mp4"
        }

        self.assertDictEqual(recordings, expected)